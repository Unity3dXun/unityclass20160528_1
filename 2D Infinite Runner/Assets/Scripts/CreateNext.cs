﻿using UnityEngine;
using System.Collections;

public class CreateNext : MonoBehaviour {

	
	
	void OnTriggerEnter2D (Collider2D other)
	{
		if(other.name == "End")
		{
			GameObject clone = Instantiate<GameObject>(other.transform.root.gameObject);
			clone.name = other.transform.root.gameObject.name;
			clone.transform.position = other.transform.position;
		}
	}
	
}
