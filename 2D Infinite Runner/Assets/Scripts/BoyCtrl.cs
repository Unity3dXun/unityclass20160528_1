﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets._2D;


public class BoyCtrl : MonoBehaviour {
	
	PlatformerCharacter2D m_Character;
	bool jump = false;
	float speed;
	
	// Use this for initialization
	void Start () {
		m_Character = GetComponent<PlatformerCharacter2D>();
	}
	
	void Update()
	{
		if(!jump)
			jump = Input.GetMouseButtonDown(0);	
		
		speed = Input.GetMouseButtonDown(1) ?  1 : 0.5f ;
	}
	
	
	// Update is called once per frame
	void FixedUpdate () {
		m_Character.Move(speed,false,jump);
		jump = false;
	}
}
