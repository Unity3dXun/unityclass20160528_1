﻿using UnityEngine;
using System.Collections;

public class MyHelloWorld : MonoBehaviour {

	public Animator myAnimator;
	public bool isHorizontalComponent = false;
	void Awake () {
		myAnimator = GetComponent<Animator> ();
		Debug.Log ("Hello World! " + name +" Awake Time="+Time.realtimeSinceStartup);
	}

	// Use this for initialization
	void Start () {
		Debug.Log ("Hello World! " + name +" Start Time="+Time.realtimeSinceStartup);
		Renderer myRenderer = gameObject.GetComponent<Renderer> ();
		if(myRenderer!=null)
			myRenderer.material.color = Color.red;
	}
	
	// Update is called once per frame
	public void Update()
	{
		if(Input.GetButtonDown("Fire1"))
		{
			isHorizontalComponent = !isHorizontalComponent;
			myAnimator.SetBool ("IsHorizontal", isHorizontalComponent);
			Debug.Log ("Hello World! isHorizontal=" + isHorizontalComponent); 
		}
	}
}
