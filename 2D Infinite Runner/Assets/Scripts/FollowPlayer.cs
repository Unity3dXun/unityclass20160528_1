﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {
	[SerializeField] float m_SpeedFactor = 1;
	
	GameObject m_Player;
	Vector3 m_PlayerLastPosition;
	
	// Use this for initialization
	void Start () {
		m_Player = GameObject.FindGameObjectWithTag("Player");
		m_PlayerLastPosition = m_Player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 deltaPostion = m_Player.transform.position - m_PlayerLastPosition;
		transform.Translate( deltaPostion.x * m_SpeedFactor , 0 , 0);		
		m_PlayerLastPosition = m_Player.transform.position;
	}
}
