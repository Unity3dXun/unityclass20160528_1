﻿using UnityEngine;
using System.Collections;

public class MyRaycastTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	GameObject lastHittenGameObject;
	bool isHold;
	Vector3 clickedPosition;
	Vector3 deltaPosition;
	public GameObject dummyGameObject;
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonDown(0))
		{
			//Debug.Log("Pressed left click. Down");
			
			//Debug.Log("Mouse Position = " + Input.mousePosition);
			
			
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			
			if (Physics.Raycast(ray, out hit, 100)) 
			{
				if(lastHittenGameObject != null)
				{
					Renderer lastRenderer = lastHittenGameObject.GetComponent<Renderer>();
					lastRenderer.material.color = Color.green;
				}
					
				Debug.DrawLine(ray.origin, hit.point,Color.yellow);		
				Debug.Log(" Hitten GameObject = " + hit.collider.gameObject.name);
				Renderer myRenderer = hit.collider.gameObject.GetComponent<Renderer>();
				clickedPosition =  hit.point;
				dummyGameObject.transform.position = clickedPosition;
				myRenderer.material.color = Color.red;
				lastHittenGameObject =  hit.collider.gameObject;
				lastHittenGameObject.transform.parent = dummyGameObject.transform;
				isHold = true;
			}
		}
		

		if (Input.GetMouseButtonUp(0))
		{
			Debug.Log("Pressed left click. Up");
			isHold = false;
			lastHittenGameObject.transform.parent = null;
		}
		
		
		
		
		if(isHold)
		{
			Debug.Log("isHold = "+ isHold + "Time= "+Time.time);
			Vector3 normalOfPlane =  Camera.main.transform.forward;
			Plane p = new Plane(normalOfPlane.normalized,clickedPosition);
			
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float rayDistance;
			if (p.Raycast(ray, out rayDistance))
			{
				dummyGameObject.transform.position = ray.GetPoint(rayDistance);
			}
		}
		
		
		
	}
}
