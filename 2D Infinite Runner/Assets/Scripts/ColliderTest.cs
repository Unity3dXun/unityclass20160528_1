﻿using UnityEngine;
using System.Collections;

public class ColliderTest : MonoBehaviour {

	// Use this for initialization
	Renderer r;
	void Start () {
		r = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision c)
	{
		Debug.Log ("我被"+ c.gameObject.name +"撞到了");
	}

	void OnTriggerEnter(Collider other) {
		Debug.Log ("我被"+ other.gameObject.name +"接觸到了");
		r.material.color = Color.magenta;


	}
	void OnTriggerStay(Collider other) {
		Debug.Log ("正被"+ other.gameObject.name +"穿過中");
		//r.material.color = Color.yellow;

	}
	void OnTriggerExit(Collider other) {
		Debug.Log (""+ other.gameObject.name +"走了");
		r.material.color = Color.blue;

	}
}
