﻿using UnityEngine;
using System.Collections;

public class DestroyAll : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D other)
	{
		if(other.name == "End")
		{
			Destroy(other.transform.root.gameObject);
		}
	}
}
