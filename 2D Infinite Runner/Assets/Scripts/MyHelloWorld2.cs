﻿using UnityEngine;
using System.Collections;

public class MyHelloWorld2 : MonoBehaviour {


	void Awake () {
		Debug.Log ("Hello World2! " + name +" Awake Time="+Time.realtimeSinceStartup);
	}

	// Use this for initialization
	void Start () {
		Debug.Log ("Hello World2! " + name +" Start Time="+Time.realtimeSinceStartup);
		Renderer myRenderer = gameObject.GetComponent<Renderer> ();
		if(myRenderer!=null)
			myRenderer.material.color = Color.red;
	}

	// Update is called once per frame
	void Update () {

	}
}
